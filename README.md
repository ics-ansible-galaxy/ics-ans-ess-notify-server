# ics-ans-ess-notify-server

Ansible playbook to install [ess-notify-server](https://gitlab.esss.lu.se/ics-software/ess-notify-server).

## License

BSD 2-clause
