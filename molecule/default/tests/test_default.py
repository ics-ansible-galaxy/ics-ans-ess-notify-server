import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ess_notify_servers")


def test_ess_notify_health(host):
    cmd = host.command(
        "curl -H Host:ess-notify-server-default -k -L https://localhost/api/v2/-/health"
    )
    assert "OK" in cmd.stdout
